"""Util Object for commonly used methods/ attributes."""
import base64
import codecs
import dateutil.parser
import json
import requests
import time
from datetime import datetime

import pymysql
from google.protobuf.json_format import MessageToDict
from aws_client import get_secret_value

db_creds = get_secret_value('hathor_nodes_db')

BLOCKCHAINS = ['osmosis']

class BlockchainAccessor():
    def __init__(self, blockchain: str) -> None:
        self.blockchain = blockchain
        self.config = self._load_config()

    def _load_config(self) -> dict:
        configs = {
            'osmosis': {
                'lcd': 'http://142.132.157.153:1317/',
                'rpc': 'http://142.132.157.153:26657/',
                'denom': 'uosmo',
                'symbol': 'OSMO',
                'osmosis_ibc_denom': 'uosmo'
            },
            'bitsong': {
                'lcd': 'https://lcd-bitsong.itastakers.com/',
                'rpc': 'https://rpc-bitsong.itastakers.com/',
                'headers': None,
                'denom': 'ubtsg',
                'symbol': 'BTSG',
                'osmosis_ibc_denom': 'ibc/4E5444C35610CC76FC94E7F7886B93121175C28262DDFDDE6F84E82BF2425452'
            },
            'lum': {
                'lcd': 'https://node0.mainnet.lum.network/rest/',
                'rpc': 'https://node0.mainnet.lum.network/rpc/',
                'headers': None,
                'denom': 'ulum',
                'symbol': 'LUM',
                'osmosis_ibc_denom': 'ibc/8A34AF0C1943FD0DFCDE9ADBF0B2C9959C45E87E6088EA2FC6ADACD59261B8A2'
            }
        }
        return configs[self.blockchain]

    def _connect_to_db(self) -> None:
        """Connect to RDS instance and store conn & cursor as attributes"""
        self.conn = pymysql.connect(
            host=db_creds['host'],
            user=db_creds['username'],
            password=db_creds['password'],
            db=self.blockchain
        )

    def _send_abci_query(
        self, request_msg: object, path: str, response_msg, height: int
    ) -> dict:
        """Encode and send pre-filled protobuf msg to RPC endpoint."""
        # Some queries have no data to pass.
        if request_msg:
            request_msg = codecs.encode(request_msg.SerializeToString(), 'hex')
            request_msg = str(request_msg, 'utf-8')

        req = {
            "jsonrpc": "2.0",
            "id": "1",
            "method": "abci_query",
            "params": {
                "height": str(height),
                "path": path,
                "data": request_msg
            }
        }
        req = json.dumps(req)
        response = requests.post(self.config['rpc'], req).json()
        if 'result' not in response:
            print(response)
        response = response['result']['response']['value']
        response = base64.b64decode(response)
        result = response_msg()
        result.ParseFromString(response)
        result = MessageToDict(result)
        return result

    def _send_query(self, method, params):
        req = {"jsonrpc": "2.0", "id": "1", "method": method,"params": params}
        req = json.dumps(req)
        response = requests.post(self.config['rpc'], req).json()
        if 'result' not in response:
            print(response)
        return response

    def _get_block_time_by_height(self, height):
        block = self._send_query('block', {"height": str(height)})
        t = dateutil.parser.isoparse(block['result']['block']['header']['time'])
        time_tuple = time.mktime(t.timetuple())
        return int(time_tuple)

    def _get_current_height(self):
        block = self._send_query("status", {})
        return int(block['result']['sync_info']['latest_block_height'])

    def get_block_by_utc_time(self, t):
        if type(t) ==  str :
            tm = dateutil.parser.isoparse(t)
            time_tuple = time.mktime(tm.timetuple())
            t = int(time_tuple)

        # Check if time is before begining of osmosis or after current time
        curr_time = int(time.mktime(datetime.now().timetuple()))
        assert t > 1624050000 and t < curr_time, "Invalid time provided."
        start = 1
        end = self._get_current_height()
        count = 0
        while start <= end :
            count += 1
            mid = (start + end) // 2
            block_time = self._get_block_time_by_height(mid)
            if block_time == t:
                return mid
            elif block_time < t:
                start = mid + 1
            else:
                end = mid - 1
        return mid
