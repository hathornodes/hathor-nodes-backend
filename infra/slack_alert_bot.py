"""Check if node is signing blocks, send slack message otherwise."""
import json
import requests
import time

from utils import BlockchainAccessor
from aws_client import get_secret_value

def run_alert_bot(rpc: str, sig: str, slack_hook: str) -> None:
    """Check latest block for the given signature and alert if missing."""
    last_missed_height = 0
    while True:
        time.sleep(5)
        try:
            block = requests.get(f'{rpc}/block').json()['result']['block']
        except Exception as e:
            requests.post(
                url=slack_hook,
                data=json.dumps({'text': f'RPC request failed!'})
            )
        if sig not in block['last_commit']['signatures']:
            height = int(block['header']['height'])
            if height != last_missed_height:
                last_missed_height = height
                requests.post(
                    url=slack_hook,
                    data=json.dumps({'text': f'Block {height} missed!'})
                )


if __name__ == '__main__':
    rpc_endpoint = BlockchainAccessor('osmosis').config['rpc']
    HATHOR_SIG = 'CEFE7D654B523DEA2A9ED718A591126C74171689'
    slack_hook = get_secret_value('node_alert_slack_hook')['node_alert_hook']
    run_alert_bot(rpc_endpoint, HATHOR_SIG, slack_hook)
