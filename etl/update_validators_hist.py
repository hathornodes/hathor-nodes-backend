"""Store Validator info on DB."""
import time

import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking

from utils import BLOCKCHAINS, BlockchainAccessor

class HistValidatorInfo(BlockchainAccessor):
    """Object for updating validator info."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)
        self.update_validators()

    def update_validators(self) -> None:
        """Update validator info at each daily epoch not in DB table."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()

        self.cursor.execute("""
            SELECT epoch, height, start_time FROM epochs e
            WHERE e.epoch NOT IN (SELECT DISTINCT epoch FROM hist_validators);
        """)
        epochs = self.cursor.fetchall()

        self.cursor.execute("""
            SELECT status, description FROM validator_status
        """)
        status_map = self.cursor.fetchall()
        status_map = {status[1]: status[0] for status in status_map}

        # Fetch and commit updated information for all validators.
        request_msg = query_staking.QueryValidatorsRequest()
        request_msg.pagination.limit = 10000
        response_msg = query_staking.QueryValidatorsResponse

        for epoch_info in epochs:
            time.sleep(5)
            if epoch_info[1] == 1:
                continue
            validators = self._send_abci_query(
                request_msg, '/cosmos.staking.v1beta1.Query/Validators',
                response_msg, epoch_info[1])['validators']

            validators_info = [None] * len(validators)
            epoch = epoch_info[0]

            for idx, val in enumerate(validators):
                val_description = val['description']
                val_commission = val['commission']['commissionRates']
                validators_info[idx] = {
                    'epoch': epoch,
                    'operator_address': val['operatorAddress'],
                    'consensus_pubkey': val['consensusPubkey']['key'],
                    'jailed': bool(val.get('jailed')),
                    '`status`': status_map[val['status']],
                    'tokens': val['tokens'],
                    'delegator_shares': float(val['delegatorShares']),
                    'moniker': val_description.get('moniker'),
                    '`identity`': val_description.get('identity'),
                    'website': val_description.get('website'),
                    'security_contact': val_description.get('security_contact'),
                    'details': val_description.get('details'),
                    'unbonding_height': val.get('unbondingHeight'),
                    'unbonding_time': val.get('unbondingTime'),
                    'commission_rate': val_commission['rate'],
                    'max_commission_rate': val_commission['maxRate'],
                    'max_change_rate': val_commission['maxChangeRate'],
                    'commission_update_time': val['commission']['updateTime'],
                    'min_self_delegation': val['minSelfDelegation']
                }
            insert_query = (
                "INSERT INTO hist_validators ("
                "epoch, operator_address, consensus_pubkey, jailed, `status`, tokens,"
                "delegator_shares, moniker, `identity`, website,"
                "security_contact, details, unbonding_height, unbonding_time,"
                "commission_rate, max_commission_rate, max_change_rate,"
                "commission_update_time, min_self_delegation) "
                "VALUES (%(epoch)s, %(operator_address)s, %(consensus_pubkey)s, %(jailed)s,"
                "%(`status`)s, %(tokens)s, %(delegator_shares)s, %(moniker)s,"
                "%(`identity`)s, %(website)s, %(security_contact)s, %(details)s,"
                "%(unbonding_height)s, %(unbonding_time)s, %(commission_rate)s,"
                "%(max_commission_rate)s, %(max_change_rate)s,"
                "%(commission_update_time)s, %(min_self_delegation)s);"
            )
            self.cursor.executemany(insert_query, validators_info)
            self.conn.commit()

        self.conn.close()
        self.cursor.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        HistValidatorInfo(chain)

