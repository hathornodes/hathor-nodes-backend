"""Get the voting history of Osmosis Validators from Cosmostation API."""
import requests

import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.distribution.v1beta1.distribution_pb2
import interface.cosmos.gov.v1beta1.query_pb2 as query_gov
import interface.cosmos.params.v1beta1.params_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking
import interface.ibc.core.client.v1.client_pb2
import interface.osmosis.pool_incentives.v1beta1.gov_pb2
import interface.osmosis.superfluid.gov_pb2

from utils import BLOCKCHAINS, BlockchainAccessor

class ValidatorVotes(BlockchainAccessor):
    """Object for updating validator votes."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)

        self.user_agent = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                        'AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/96.0.4664.55 Safari/537.36')
        self.headers = {
            'User-Agent': self.user_agent,
            'referer': 'https://www.mintscan.io/',
            'origin': 'https://www.mintscan.io'}
        self.update_validator_votes()

        # Cephalopod Equipment Corp Auth Voting
        # TODO: Use 'authz discovery' to find all the validators that need this
        if self.blockchain == 'osmosis':
            self.correct_authz_votes(
                auth_address = 'osmo16u7kmc7wr9cdtg55j3zym22lmfrv3cff6vwg0s',
                account_address = 'osmo1x20lytyf6zkcrv5edpkfkn8sz578qg5saxene9'
            )

    def _get_most_recent_proposal(self) -> int:
        """Query blockchain for active proposals and return largest id."""
        request_msg = query_gov.QueryProposalsRequest()
        request_msg.proposal_status = 2
        request_msg.pagination.limit = 1000
        proposals = self._send_abci_query(
            request_msg, '/cosmos.gov.v1beta1.Query/Proposals',
            query_gov.QueryProposalsResponse, 0)

        proposals = proposals.get('proposals', None)
        if proposals:
            curr_proposal_id = max([int(proposal['proposalId']) for proposal in proposals])
        else:
            curr_proposal_id = 0
        return curr_proposal_id

    def update_validator_votes(self) -> None:
        """Get voting history, voting power, & start date for all validators."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute("""
            SELECT
                MIN(p.id),
                MAX(p.id)
            FROM
                proposals p
            WHERE
                p.voting_end_time >= NOW() - INTERVAL 1 DAY
                OR p.id NOT IN (
            SELECT
                DISTINCT proposal_id
            FROM
                `_validator_votes`);
        """)
        start_prop, end_prop = self.cursor.fetchall()[0]
        active_proposal_cap = self._get_most_recent_proposal()

        assert end_prop or active_proposal_cap, "Proposal fetching failed."
        if end_prop:
            end_prop = max(active_proposal_cap, end_prop)
        elif active_proposal_cap:
            end_prop = active_proposal_cap

        if not start_prop:
            start_prop = end_prop
        proposals = range(start_prop, end_prop+1)
        self.cursor.execute(
            'SELECT operator_address, account_address FROM _validators')
        validators = self.cursor.fetchall()
        address_validator_map = {
            validator[1]: validator[0] for validator in validators}

        self.cursor.execute('SELECT vote_option, description FROM vote_options')
        vote_options = self.cursor.fetchall()
        vote_options_map = {option[1]: option[0] for option in vote_options}
        for prop_id in proposals:
            url = (f'https://api.mintscan.io/v1/{self.blockchain}/proposals/'
                    f'{prop_id}/votes/validators')
            votes = requests.get(url, headers=self.headers).json()
            try:
                validator_votes = [
                    {
                        'operator_address': address_validator_map[vote['voter']],
                        'proposal_id': prop_id,
                        'vote_option': vote_options_map.get(vote['option'], None)
                    }
                    for vote in votes
                ]

                insert_query = (
                    'INSERT INTO '
                    '_validator_votes (operator_address, proposal_id, vote_option) '
                    'VALUES(%(operator_address)s, %(proposal_id)s, %(vote_option)s) '
                    'ON DUPLICATE KEY UPDATE vote_option = VALUES(vote_option)'
                )
                self.cursor.executemany(insert_query, validator_votes)
                self.conn.commit()
            except Exception as error:
                print(prop_id, error)

        self.cursor.close()
        self.conn.close()

    def correct_authz_votes(self, account_address, auth_address) -> None:
        """Find votes cast by an authorized wallet on behalf of a validator."""
        # Pull all auth transactions.
        url = '{lcd}{path}{events}%27{auth_address}%27&pagination.limit=0'.format(
            lcd=self.config['lcd'],
            path='cosmos/tx/v1beta1/txs?events=',
            events='message.sender=',
            auth_address=auth_address)

        auth_txs = requests.get(url).json()['txs']

        # Pull vote option & account address maps from DB.
        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute(
            'SELECT operator_address, account_address FROM _validators')
        validators = self.cursor.fetchall()
        address_validator_map = {
            validator[1]: validator[0] for validator in validators}
        self.cursor.execute('SELECT vote_option, description FROM vote_options')
        vote_options = self.cursor.fetchall()
        vote_options_map = {option[1]: option[0] for option in vote_options}

        # Reformat vote auth txs using maps.
        operator_address = address_validator_map[account_address]
        votes = []
        for transaction in auth_txs:
            tx_message = transaction['body']['messages'][0]
            msg_type = tx_message['@type']
            if msg_type == '/cosmos.gov.v1beta1.MsgVote':
                voter = address_validator_map.get(tx_message['voter'], None)
                if voter == operator_address:
                    votes.append[{
                        'operator_address': operator_address,
                        'proposal_id': tx_message['proposal_id'],
                        'vote_option': vote_options_map[tx_message['option']]
                    }]
            elif msg_type == '/cosmos.authz.v1beta1.MsgExec':
                for exec_msg in tx_message['msgs']:
                    if exec_msg['@type'] != '/cosmos.gov.v1beta1.MsgVote':
                        continue

                    valid_voter  = address_validator_map.get(
                        exec_msg['voter'], None)
                    if valid_voter:
                        votes.append({
                            'operator_address': valid_voter,
                            'proposal_id': exec_msg['proposal_id'],
                            'vote_option': vote_options_map[exec_msg['option']]
                        })

        # Insert into DB.
        insert_query = (
            'INSERT INTO '
            '   _validator_votes (operator_address, proposal_id, vote_option)'
            'VALUES(%(operator_address)s, %(proposal_id)s, %(vote_option)s) '
            'ON DUPLICATE KEY UPDATE vote_option = VALUES(vote_option)')
        self.cursor.executemany(insert_query, votes)
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        ValidatorVotes(chain)

