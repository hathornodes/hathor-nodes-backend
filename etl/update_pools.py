"""Store pool data for all pools at every Epoch into DB."""
import codecs
import datetime

import interface.osmosis.gamm.v1beta1.query_pb2 as query_gamms
from utils import BLOCKCHAINS, BlockchainAccessor

class PoolInfo(BlockchainAccessor):
    """Object for updating pools info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.update_pools()

    def _find_first_height(self, p_id, start_time, end_time):
        """Find and return when the pool id first appears in blockchain"""
        request_msg = query_gamms.QueryPoolRequest()
        request_msg.poolId = p_id
        request_msg = codecs.encode(request_msg.SerializeToString(), 'hex')
        request_msg = str(request_msg, 'utf-8')

        while start_time < end_time:
            mid = (start_time + end_time) // 2

            pool_at_mid_code = self._send_query(
                "abci_query",
                {
                    "height": str(mid),
                    "path": '/osmosis.gamm.v1beta1.Query/Pool',
                    "data": request_msg
                }
            )['result']['response']['code']

            if pool_at_mid_code == 0:
                end_time = mid
            else:
                start_time = mid + 1
        # Once the initial height has been found, get the pool and
        # extract the neccessary information for the table
        request_msg = query_gamms.QueryPoolRequest()
        request_msg.poolId = p_id
        response_msg = query_gamms.QueryPoolResponse
        pool = self._send_abci_query(
            request_msg, '/osmosis.gamm.v1beta1.Query/Pool',
            response_msg, end_time).get('pool')

        block_time = self._get_block_time_by_height(end_time)
        block_time = datetime.datetime.fromtimestamp(block_time)
        pool_info = {
            'denom': pool.get('totalShares')['denom'],
            'address': pool['address'],
            'type': pool['@type'],
            'creation_time': block_time,
            'creation_height': end_time
        }
        return pool_info

    def update_pools(self) -> None:
        """Fetch and commit updated information for all pools."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()

        self.cursor.execute("""
            SELECT
                pool_info.id,
                start_height.height AS start_height,
                end_height.height AS end_height
            FROM
                (
                SELECT
                    hp.id,
                    MIN(hp.epoch) AS upper_bound_epoch
                FROM
                    hist_pools hp
                GROUP BY
                    hp.id) pool_info
            LEFT JOIN epochs end_height ON
                pool_info.upper_bound_epoch = end_height.epoch
            LEFT JOIN epochs start_height ON
                pool_info.upper_bound_epoch = start_height.epoch + 1
            WHERE
                pool_info.id NOT IN (SELECT DISTINCT `id` FROM pools);
            """)

        missing_pools = self.cursor.fetchall()
        self.cursor.close()
        self.conn.close()

        pools_info = [None] * len(missing_pools)

        for idx, pool in enumerate(missing_pools):
            pool_id, start_time, end_time = pool
            pool_info = self._find_first_height(pool_id, start_time, end_time)

            pools_info[idx] = {
                '`id`': pool_id,
                'address': pool_info['address'],
                'denom': pool_info['denom'],
                '`@type`': pool_info['type'],
                'creation_height': pool_info['creation_height'],
                'creation_time': pool_info['creation_time']
            }
        insert_query = (
            "INSERT INTO pools (`id`, address, denom,"
            "`@type`, creation_height, creation_time)"
            "VALUES (%(`id`)s, %(address)s, %(denom)s,"
            "%(`@type`)s, %(creation_height)s, %(creation_time)s);"
        )
        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.executemany(insert_query, pools_info)
        self.conn.commit()

        self.conn.close()
        self.cursor.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        PoolInfo(chain)
