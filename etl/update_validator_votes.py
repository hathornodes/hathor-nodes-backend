"""Store validator votes in DB."""
import time
from datetime import datetime

import interface.cosmos.gov.v1beta1.query_pb2 as query_gov
import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking

from utils import BLOCKCHAINS, BlockchainAccessor

class ValidatorVotes(BlockchainAccessor):
    """Object for updating validator votes."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)
        self.update_validator_votes()

    def update_validator_votes(self) -> None:
        """Fetch and commit updated voting records for all validators."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()

        # Get validator info.
        self.cursor.execute(
            'SELECT operator_address, account_address, update_time FROM _validators'
        )
        validators = self.cursor.fetchall()

        self.cursor.execute("""
            SELECT
                `id`, voting_end_time
            FROM
                proposals p
            WHERE
                voting_end_time > NOW() - INTERVAL 1 DAY;
        """)
        proposals = self.cursor.fetchall()

        self.cursor.execute('SELECT vote_option, description FROM vote_options')
        vote_options = self.cursor.fetchall()
        vote_options = {option[1]: option[0] for option in vote_options}
        voters = {validator[1]: validator[0] for validator in validators}

        all_votes = []
        for prop in proposals:
            prop_id, voting_end_time = prop
            if voting_end_time > datetime.now():
                query_height = self._get_current_height()
            else:
                end_time = time.mktime(voting_end_time.timetuple())
                query_height = self.get_block_by_utc_time(end_time)

            # TODO: Test performance of both methods
            # 1: Query RPC for all votes and look for validator votes.
            # 2: Query RPC for each validator's vote per prop.
            # Currently implementing method 1
            votes = query_gov.QueryVotesRequest()
            votes.proposal_id = prop_id
            votes.pagination.limit = 10000000

            prop_votes = self._send_abci_query(
                votes, '/cosmos.gov.v1beta1.Query/Votes',
                query_gov.QueryVotesResponse, query_height)
            prop_votes = prop_votes.get('votes', None)

            if not prop_votes:
                print(prop_id, "Query Height failed to fetch votes")
                prop_votes = self._send_abci_query(
                    votes, '/cosmos.gov.v1beta1.Query/Votes',
                    query_gov.QueryVotesResponse, query_height-1)
                prop_votes = prop_votes.get('votes', [])
                if not prop_votes:
                    print(prop_id, "Query Height Minus 1 Failed, skipping")

            for vote in prop_votes:
                operator_address = voters.get(vote['voter'], None)
                if operator_address:
                    all_votes.append(
                        {
                            'operator_address': operator_address,
                            'proposal_id': vote['proposalId'],
                            'vote_option': vote_options[vote['option']]
                        }
                    )

        insert_query = (
            'INSERT INTO '
            '   validator_votes (operator_address, proposal_id, vote_option)'
            'VALUES(%(operator_address)s, %(proposal_id)s, %(vote_option)s) '
            'ON DUPLICATE KEY UPDATE vote_option = VALUES(vote_option)'
        )
        self.cursor.executemany(insert_query, all_votes)
        self.cursor.close()
        self.conn.commit()
        self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        ValidatorVotes(chain)

