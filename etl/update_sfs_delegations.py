"""Collect data on SFS Delegations."""
import codecs
import re
import requests

import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking
import interface.cosmos.tx.v1beta1.service_pb2 as query_txs
import interface.osmosis.lockup.lock_pb2
import interface.osmosis.lockup.query_pb2 as query_locks
import interface.osmosis.gamm.v1beta1.query_pb2 as query_gamms
import interface.osmosis.superfluid.query_pb2 as query_superfluid
from utils import BlockchainAccessor

class SFSInfo(BlockchainAccessor):
    """Object for updating pools info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.pool_info = {}
        self.update_sfs_info()

    def _fetch_sfs_delegations(self, epoch_info) -> list:
        """Query all sfs delegations."""
        request_msg = query_staking.QueryValidatorsRequest()
        request_msg.pagination.limit = 10000
        validators = self._send_abci_query(
            request_msg, '/cosmos.staking.v1beta1.Query/Validators',
            query_staking.QueryValidatorsResponse, epoch_info[1])['validators']

        request_msg = query_superfluid.AllAssetsRequest()
        sfs_assets = self._send_abci_query(
            request_msg, '/osmosis.superfluid.Query/AllAssets',
            query_superfluid.AllAssetsResponse, epoch_info[1]).get('assets', [])
        sfs_txs = []
        for validator in validators:
            for asset in sfs_assets:
                request_msg = query_superfluid.SuperfluidDelegationsByValidatorDenomRequest()
                request_msg.validator_address = validator['operatorAddress']
                request_msg.denom = asset['denom']
                sfs_delegators = self._send_abci_query(
                    request_msg, '/osmosis.superfluid.Query/SuperfluidDelegationsByValidatorDenom',
                    query_superfluid.SuperfluidDelegationsByValidatorDenomResponse, epoch_info[1])
                sfs_delegators = sfs_delegators.get('superfluidDelegationRecords', [])

                for delegator in sfs_delegators:
                    sfs_txs.append({
                        'epoch': epoch_info[0],
                        'operator_address': validator['operatorAddress'],
                        'delegator_address': delegator['delegatorAddress'],
                        'denom': delegator['delegationAmount']['denom'],
                        'amount': delegator['delegationAmount']['amount']
                    })
        return sfs_txs

    def _calc_delegation_values(
            self, epoch_info: tuple, delegations: list) -> list:
        """Query Num Gamms in each SFS Lockup & calc # Osmos underlying."""
        for delegation in delegations:
            pool_id = re.search(
                '(?<=gamm/pool/)[0-9]*', delegation['denom']).group(0)
            if pool_id in self.pool_info:
                pool_info = self.pool_info[pool_id]
            else:
                request_msg = query_gamms.QueryPoolRequest()
                request_msg.poolId = int(pool_id)
                pool_info = self._send_abci_query(
                    request_msg, '/osmosis.gamm.v1beta1.Query/Pool',
                    query_gamms.QueryPoolResponse, epoch_info[1])['pool']
                self.pool_info[pool_id] = pool_info

            for asset in pool_info['poolAssets']:
                if asset['token']['denom'] == 'uosmo':
                    n_osmos = int(asset['token']['amount'])/1000000
                    break

            gamms = int(delegation['amount'])
            total_gamms = int(pool_info['totalShares']['amount'])
            osmos_per_gamm = n_osmos/total_gamms
            delegation['osmo_amount'] = osmos_per_gamm * gamms/2
        return delegations

    def update_sfs_info(self) -> None:
        """Fetch and commit updated information for all SFS Delegations."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute("""
            SELECT epoch, height FROM epochs e
            WHERE e.epoch >= 256
            AND e.epoch NOT IN (SELECT DISTINCT epoch FROM hist_sfs_delegations);
        """)

        insert_query = (
            "INSERT INTO hist_sfs_delegations ("
            "epoch, operator_address, delegator_address, denom, amount, osmo_amount) "
            "VALUES (%(epoch)s, %(operator_address)s, %(delegator_address)s, "
            "%(denom)s, %(amount)s, %(osmo_amount)s) "
            "ON DUPLICATE KEY UPDATE osmo_amount=VALUES(osmo_amount)"
        )
        epoch_info = self.cursor.fetchall()
        for epoch in epoch_info:
            print(epoch)
            sfs_del = self._fetch_sfs_delegations(epoch)
            sfs_del = self._calc_delegation_values(epoch, sfs_del)
            self.cursor.executemany(insert_query, sfs_del)
            self.conn.commit()

        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    SFSInfo('osmosis')
