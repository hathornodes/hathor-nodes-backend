"""Store pool data for all pools at every Epoch into DB."""
import time

import re

import interface.osmosis.gamm.v1beta1.pool_pb2
import interface.osmosis.gamm.v1beta1.query_pb2 as query_gamms

from utils import BLOCKCHAINS, BlockchainAccessor

class HistPoolInfo(BlockchainAccessor):
    """Object for updating proposal info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.update_pools()

    def update_pools(self) -> None:
        """Fetch and commit updated information for all pools."""
        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute("""
            SELECT epoch, height, start_time FROM epochs e
            WHERE e.epoch NOT IN (SELECT DISTINCT epoch FROM hist_pools);
        """)
        epochs = self.cursor.fetchall()

        request_msg = query_gamms.QueryPoolsRequest()
        request_msg.pagination.limit = 10000
        response_msg = query_gamms.QueryPoolsResponse

        for epoch_info in epochs:
            time.sleep(5)
            if epoch_info[1] == 1:
                continue
            pools = self._send_abci_query(
                request_msg, '/osmosis.gamm.v1beta1.Query/Pools',
                response_msg, epoch_info[1])['pools']
            pool_info = [None] * len(pools)
            pool_assets = []

            for idx, pool in enumerate(pools):
                pool_params = pool.get('poolParams')
                pool_shares = pool.get('totalShares')
                pool_id = re.search(
                    '(?<=gamm/pool/)[0-9]*', pool_shares['denom']).group(0)
                pool_info[idx] = {
                    'epoch': epoch_info[0],
                    '`id`': pool_id,
                    'address': pool['address'],
                    'swap_fee': pool_params.get('swapFee'),
                    'exit_fee': pool_params.get('exitFee'),
                    'future_pool_governor': pool.get('futurePoolGovernor'),
                    'total_shares': pool_shares['amount'],
                    'total_weight': pool['totalWeight']
                }

                for asset in pool['poolAssets']:
                    asset_info = asset['token']
                    pool_assets.append({
                        'epoch': epoch_info[0],
                        'pool_id': pool_id,
                        'denom': asset_info['denom'],
                        'amount': asset_info['amount'],
                        'weight': asset['weight']
                    })

            pool_insert_query = (
                "INSERT INTO hist_pools ("
                "epoch, `id`, address, swap_fee, exit_fee,"
                "future_pool_governor, total_shares, total_weight) "
                "VALUES (%(epoch)s, %(`id`)s, %(address)s,"
                "%(swap_fee)s, %(exit_fee)s, %(future_pool_governor)s,"
                "%(total_shares)s, %(total_weight)s) "
                "ON DUPLICATE KEY UPDATE total_shares = VALUES(total_shares)"
            )
            self.cursor.executemany(pool_insert_query, pool_info)
            self.conn.commit()

            pool_asset_insert_query = (
                "INSERT INTO hist_pool_assets ("
                "epoch, pool_id, denom, amount, weight) VALUES ("
                "%(epoch)s, %(pool_id)s, %(denom)s, %(amount)s, %(weight)s) "
                "ON DUPLICATE KEY UPDATE amount = VALUES(amount)"
            )
            self.cursor.executemany(pool_asset_insert_query, pool_assets)
            self.conn.commit()

        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        HistPoolInfo(chain)
