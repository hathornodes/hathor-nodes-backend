"""Store proposal info into DB."""
import time

import dateutil.parser
import requests

from utils import BLOCKCHAINS, BlockchainAccessor

class ProposalInfo(BlockchainAccessor):
    """Object for updating proposal info."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)
        self.user_agent = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                        'AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/96.0.4664.55 Safari/537.36')
        self.headers = {
            'User-Agent': self.user_agent,
            'referer': 'https://www.mintscan.io/',
            'origin': 'https://www.mintscan.io'}
        self.update_proposals()

    def update_proposals(self) -> None:
        """Fetch and commit updated information for all validators."""
        url = f'https://api.mintscan.io/v1/{self.blockchain}/proposals'
        proposals = requests.get(url, headers=self.headers).json()

        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute("""
            SELECT status, description FROM proposal_status
        """)
        status_map = self.cursor.fetchall()
        status_map = {status[1]: status[0] for status in status_map}

        self.cursor.execute(
            'SELECT `id`, voting_end_height '
            'FROM proposals '
            'WHERE voting_end_time > NOW() - INTERVAL 4 DAY'
        )
        active_props = self.cursor.fetchall()
        active_props = {prop[0]: prop[1] for prop in active_props}
        if active_props:
            last_active_prop = max(active_props.keys())
        else:
            last_active_prop = max([int(prop['id']) for prop in proposals])

        reformatted_proposals = []

        for base_prop in proposals:
            prop = base_prop.copy()

            prop_id = int(prop.pop('id'))
            if prop_id < last_active_prop and prop_id not in active_props:
                continue

            deposit_info = prop.get('total_deposit')
            if deposit_info:
                deposit = int(deposit_info[0]['amount'])
            else:
                deposit = None

            prop['`id`'] = prop_id
            prop['`@type`'] = prop.pop('proposal_type')
            prop['status'] = status_map[prop.pop('proposal_status')]
            prop['`no`'] = int(prop.pop('no'))
            prop['total_deposit'] = deposit

            voting_end_height = active_props.get(prop_id, None)
            prop['voting_end_height'] = voting_end_height
            voting_end_time = prop.get('voting_end_time', None)

            # Calculate voting_end_height for new props.
            # Check that voting_end_time exists and isn't the deposit default
            if (not voting_end_height and
                    voting_end_time and
                    voting_end_time != "0001-01-01T00:00:00.000Z"):
                voting_end_time = dateutil.parser.isoparse(voting_end_time)
                voting_end_time = time.mktime(voting_end_time.timetuple())
                voting_end_time = int(voting_end_time)
                if voting_end_time < time.time():
                    prop['voting_end_height'] = self.get_block_by_utc_time(
                        voting_end_time)


            prop.pop('tx_hash')
            prop.pop('initial_deposit')
            prop.pop('moniker')
            reformatted_proposals.append(prop)

        insert_query = (
            "INSERT INTO proposals ("
            "`id`, `@type`, title, description, status, yes, abstain, `no`,"
            "no_with_veto, submit_time, deposit_end_time, total_deposit,"
            "voting_start_time, voting_end_time, voting_end_height) "
            "VALUES (%(`id`)s, %(`@type`)s, %(title)s, %(description)s,"
            "%(status)s, %(yes)s, %(abstain)s, %(`no`)s, %(no_with_veto)s,"
            "%(submit_time)s, %(deposit_end_time)s, %(total_deposit)s,"
            "%(voting_start_time)s, %(voting_end_time)s, %(voting_end_height)s) "
            "ON DUPLICATE KEY UPDATE status = VALUES(status),"
            "yes = VALUES(yes), abstain = VALUES(abstain),"
            "`no` = VALUES(`no`), no_with_veto = VALUES(no_with_veto),"
            "voting_end_height = VALUES(voting_end_height),"
            "total_deposit = VALUES(total_deposit);"
        )

        if reformatted_proposals:
            self.conn.ping()
            self.cursor.executemany(insert_query, reformatted_proposals)
            self.conn.commit()

            self.cursor.close()
            self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        ProposalInfo(chain)
