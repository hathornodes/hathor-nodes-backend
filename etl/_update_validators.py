"""Get the status of Osmosis Validators from Cosmostation API."""
import requests

from utils import BLOCKCHAINS, BlockchainAccessor

class ValidatorInfo(BlockchainAccessor):
    """Object for updating validator votes."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)
        self.user_agent = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) '
                        'AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/96.0.4664.55 Safari/537.36')
        self.headers = {
            'User-Agent': self.user_agent,
            'referer': 'https://www.mintscan.io/',
            'origin': 'https://www.mintscan.io'}
        self.update_validator_info()

    def update_validator_info(self) -> None:
        """Get validator info from Cosmostation API."""
        url = f'https://api-{self.blockchain}.cosmostation.io/v1/staking/validators'
        validators = requests.get(url, headers=self.headers).json()

        for val in validators:
            val['consensus_address'] = val['uptime']['address']
            val['`status`'] = val.pop('status')
            val['`identity`'] = val.pop('identity')
            val['tokens'] = int(val['tokens'])
            val['delegator_shares'] = float(val['delegator_shares'])
            val['commission_rate'] = float(val.pop('rate'))
            val['max_commission_rate'] = float(val.pop('max_rate'))
            val['max_change_rate'] = float(val['max_change_rate'])
            val['commission_update_time'] = val['update_time']
            val['unbonding_height'] = int(val['unbonding_height'])

            val.pop('rank')
            val.pop('uptime')
            val.pop('keybase_url')

        insert_query = (
            "INSERT INTO _validators ("
            "account_address, consensus_address,"
            "operator_address, consensus_pubkey, jailed, `status`, tokens,"
            "delegator_shares, moniker, `identity`, website,"
            "details, unbonding_height, unbonding_time,"
            "commission_rate, max_commission_rate, max_change_rate, update_time,"
            "commission_update_time, min_self_delegation) "
            "VALUES (%(account_address)s, %(consensus_address)s,"
            "%(operator_address)s, %(consensus_pubkey)s, %(jailed)s,"
            "%(`status`)s, %(tokens)s, %(delegator_shares)s, %(moniker)s,"
            "%(`identity`)s, %(website)s, %(details)s,"
            "%(unbonding_height)s, %(unbonding_time)s, %(commission_rate)s,"
            "%(max_commission_rate)s, %(max_change_rate)s, %(update_time)s,"
            "%(commission_update_time)s, %(min_self_delegation)s)"
            " ON DUPLICATE KEY UPDATE `status` = VALUES(`status`),"
            "account_address = VALUES(`account_address`),"
            "jailed = VALUES(jailed), tokens = VALUES(tokens),"
            "delegator_shares = VALUES(delegator_shares),"
            "moniker = VALUES(moniker), `identity` = VALUES(`identity`),"
            "website = VALUES(website),"
            "details = VALUES(details),"
            "unbonding_time = VALUES(unbonding_time),"
            "commission_rate  = VALUES(commission_rate),"
            "max_commission_rate = VALUES(max_commission_rate),"
            "max_change_rate = VALUES(max_change_rate),"
            "commission_update_time = VALUES(commission_update_time),"
            "update_time = VALUES(update_time),"
            "min_self_delegation = VALUES(min_self_delegation);"
        )

        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.executemany(insert_query, validators)
        self.conn.commit()

        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        ValidatorInfo(chain)
