"""Get the starting height and utc time of a blockchain's Epochs."""
import interface.cosmos.gov.v1beta1.query_pb2 as query_gov
import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking
from interface.osmosis.epochs.query_pb2 import QueryEpochsInfoResponse

from utils import BlockchainAccessor

class EpochInfo(BlockchainAccessor):
    """Upload block height & datetime of every daily epoch."""
    def __init__(self, blockchain: str) -> None:
        super().__init__(blockchain)
        self.update_epoch_info()

    def _get_epoch_info(self, height: int) -> list:
        """Get daily (idx 0) epoch info."""
        resp = QueryEpochsInfoResponse
        epoch_info = self._send_abci_query(
            '', '/osmosis.epochs.v1beta1.Query/EpochInfos',
            resp, height)['epochs'][0]
        return epoch_info

    def update_epoch_info(self) -> None:
        """Insert newer epoch info to DB Table."""
        curr_height = self._get_current_height()
        curr_epoch = self._get_epoch_info(curr_height)

        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute('SELECT MAX(epoch) FROM epochs')
        start_epoch = self.cursor.fetchall()[0][0]
        self.cursor.close()
        self.conn.close()

        n_epochs = int(curr_epoch['currentEpoch']) - start_epoch
        epochs = [None] * n_epochs

        for idx in range(n_epochs):
            epoch_info = self._get_epoch_info(curr_height)
            epoch_height = int(epoch_info.get('currentEpochStartHeight', None))
            epochs[idx] = {
                'epoch': epoch_info['currentEpoch'],
                'height': epoch_height,
                'start_time': epoch_info['currentEpochStartTime']
            }
            curr_height = epoch_height-1

        insert_query = (
            'INSERT INTO epochs (epoch, height, start_time) '
            'VALUES (%(epoch)s, %(height)s, %(start_time)s) '
        )

        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.executemany(insert_query, epochs)
        self.conn.commit()

        self.conn.close()
        self.cursor.close()

if __name__ == "__main__":
    EpochInfo('osmosis')
