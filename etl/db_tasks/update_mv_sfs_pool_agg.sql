CREATE TABLE mv_sfs_pool_agg
SELECT
    hsd.epoch,
    hsd.denom,
    COUNT(*) AS n_sfs_delegations,
    SUM(osmo_amount) AS osmo_delegated,
    ROUND(100 * SUM(hsd.amount)/ AVG(hp.total_shares), 1) AS perc_pool_sfs
FROM
    hist_sfs_delegations hsd
INNER JOIN
    pools p ON
    p.denom = hsd.denom
INNER JOIN
    hist_pools hp ON
    hp.epoch = hsd.epoch
    AND hp.id = p.id
GROUP BY
    hsd.denom,
    hsd.epoch
ORDER BY
    hsd.denom,
    hsd.epoch DESC;

CREATE EVENT Eve_tbl_MV_SFS_Validator_Pool_INSERT
ON SCHEDULE EVERY 24 HOUR
DO
INSERT INTO mv_sfs_pool_agg
SELECT
    hsd.epoch,
    hsd.denom,
    COUNT(*) AS n_sfs_delegations,
    SUM(osmo_amount) AS osmo_delegated,
    ROUND(100 * SUM(hsd.amount)/ AVG(hp.total_shares), 1) AS perc_pool_sfs
FROM
    hist_sfs_delegations hsd
INNER JOIN
    pools p ON
    p.denom = hsd.denom
INNER JOIN
    hist_pools hp ON
    hp.epoch = hsd.epoch
    AND hp.id = p.id
WHERE
    hsd.epoch NOT IN (SELECT DISTINCT epoch FROM epochs)
GROUP BY
    hsd.denom,
    hsd.epoch
ORDER BY
    hsd.denom,
    hsd.epoch DESC;
