CREATE TABLE mv_sfs_validator_agg
SELECT
    hsd.epoch,
    hsd.operator_address,
    COUNT(*) AS n_delegations,
    SUM(hsd.osmo_amount) AS osmo_delegated
FROM
    hist_sfs_delegations hsd
GROUP BY
    hsd.epoch,
    hsd.operator_address;

CREATE EVENT Eve_tbl_MV_SFS_Validator_Agg_INSERT
ON SCHEDULE EVERY 24 HOUR
DO
INSERT INTO mv_sfs_validator_agg
SELECT
    hsd.epoch,
    hsd.operator_address,
    COUNT(*) AS n_delegations,
    SUM(hsd.osmo_amount) AS osmo_delegated
FROM
    hist_sfs_delegations hsd
WHERE
    hsd.epoch NOT IN (SELECT DISTINCT epoch FROM epochs)
GROUP BY
    hsd.epoch,
    hsd.operator_address;
