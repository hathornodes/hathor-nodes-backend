"""Store token info into DB."""
import requests

from utils import BlockchainAccessor

class TokenInfo(BlockchainAccessor):
    """Object for updating proposal info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.update_tokens()

    def update_tokens(self) -> None:
        """Fetch and commit updated information for all Osmosis assets."""
        url = ('https://raw.githubusercontent.com/osmosis-labs/assetlists/'
                'main/osmosis-1/osmosis-1.assetlist.json')
        response = requests.get(url).json()
        tokens = response.get('assets', None)
        if not tokens:
            print(response)

        reformatted_tokens = [None] * len(tokens)
        reformatted_denoms = []

        for idx, token in enumerate(tokens):
            denoms = token.get('denom_units', [])
            ibc = token.get('ibc', {})

            reformatted_tokens[idx] = {
                'token': token.get('name', None),
                'symbol': token.get('symbol', None),
                'coingecko_id': token.get('coingecko_id', None),
                'display': token.get('display', None),
                'description': token.get('description', None),
                'base': token.get('base', None),
                'ibc_source_channel': ibc.get('source_channel', None),
                'ibc_dst_channel': ibc.get('dst_channel', None),
                'ibc_source_denom': ibc.get('source_denom', None)
            }

            token_denoms = []
            # Token denoms come in pairs
            # Want to reverse the exponents in them
            # TODO: Optimize or recreate process
            for denom in denoms:
                if token_denoms and token_denoms[0]:
                    token_denoms.append({
                        'token': token.get('name', None),
                        'denom': denom['denom'],
                        'exponent': token_denoms[0]['exponent']
                    })
                    token_denoms[0]['exponent'] = denom['exponent']
                else:
                    token_denoms.append({
                        'token': token.get('name', None),
                        'denom': denom['denom'],
                        'exponent': denom['exponent']
                    })
            reformatted_denoms.extend(token_denoms)

        token_insert_query = (
            "INSERT INTO tokens ("
            "token, symbol, coingecko_id, display, description, base,"
            "ibc_source_channel, ibc_dst_channel, ibc_source_denom)"
            "VALUES (%(token)s, %(symbol)s, %(coingecko_id)s, %(display)s,"
            "%(description)s, %(base)s,"
            "%(ibc_source_channel)s, %(ibc_dst_channel)s, %(ibc_source_denom)s)"
            "ON DUPLICATE KEY UPDATE "
            "coingecko_id = VALUES(coingecko_id),"
            "display = VALUES(display),"
            "description = VALUES(description),"
            "base = VALUES(base),"
            "ibc_source_channel = VALUES(ibc_source_channel),"
            "ibc_dst_channel = VALUES(ibc_dst_channel),"
            "ibc_source_denom = VALUES(ibc_source_denom);")

        denom_insert_query = (
            "INSERT INTO token_denoms (token, denom, exponent)"
            "VALUES (%(token)s, %(denom)s, %(exponent)s)"
            "ON DUPLICATE KEY UPDATE exponent = VALUES(exponent);")

        self._connect_to_db()
        self.cursor = self.conn.cursor()

        self.cursor.executemany(token_insert_query, reformatted_tokens)
        self.cursor.executemany(denom_insert_query, reformatted_denoms)
        self.conn.commit()

        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    TokenInfo('osmosis')

