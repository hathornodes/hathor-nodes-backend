"""Store proposal info into DB."""
import time

import dateutil.parser

import interface.cosmos.distribution.v1beta1.distribution_pb2
import interface.cosmos.gov.v1beta1.query_pb2 as query_gov
import interface.cosmos.params.v1beta1.params_pb2
import interface.cosmos.upgrade.v1beta1.upgrade_pb2
import interface.ibc.core.client.v1.client_pb2
import interface.osmosis.pool_incentives.v1beta1.gov_pb2

from utils import BLOCKCHAINS, BlockchainAccessor

class ProposalInfo(BlockchainAccessor):
    """Object for updating proposal info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.update_proposals()

    def update_proposals(self) -> None:
        """Fetch and commit updated information for all proposals."""
        request_msg = query_gov.QueryProposalsRequest()
        request_msg.pagination.limit = 10000
        proposals = self._send_abci_query(
            request_msg, '/cosmos.gov.v1beta1.Query/Proposals',
            query_gov.QueryProposalsResponse, 0)['proposals']

        self._connect_to_db()
        self.cursor = self.conn.cursor()
        self.cursor.execute("""
            SELECT status, description FROM proposal_status
        """)
        status_map = self.cursor.fetchall()
        status_map = {status[1]: status[0] for status in status_map}

        self.cursor.execute(
            'SELECT `id`, voting_end_height '
            'FROM proposals '
            'WHERE voting_end_time > NOW() - INTERVAL 4 DAY'
        )
        active_props = self.cursor.fetchall()
        active_props = {prop[0]: prop[1] for prop in active_props}
        if active_props:
            last_active_prop = max(active_props.keys())
        else:
            last_active_prop = max([int(prop['id']) for prop in proposals])

        reformatted_proposals = []

        proposals = [
            proposal
            for proposal in proposals
            if (int(proposal['proposalId']) > last_active_prop or
                int(proposal['proposalId']) in active_props)
        ]
        reformatted_proposals = [None] * len(proposals)

        for idx, prop in enumerate(proposals):
            prop_id = int(prop.pop('proposalId'))
            prop_content = prop['content']
            prop_tally = prop['finalTallyResult']

            voting_end_time = prop.get('votingEndTime', None)
            voting_end_height = None
            if voting_end_time:
                voting_end_time = dateutil.parser.isoparse(voting_end_time)
                voting_end_time = time.mktime(voting_end_time.timetuple())
                voting_end_time = int(voting_end_time)
                if voting_end_time < time.time():
                    voting_end_height = self.get_block_by_utc_time(
                        voting_end_time)

            reformatted_proposals[idx] = {
                '`id`': prop_id,
                '`@type`': prop_content['@type'],
                'title': prop_content['title'],
                'description': prop_content['description'],
                'status': status_map[prop['status']],
                'yes': int(prop_tally['yes']),
                'abstain': int(prop_tally['abstain']),
                '`no`': int(prop_tally['no']),
                'no_with_veto': int(prop_tally['noWithVeto']),
                'submit_time': prop['submitTime'],
                'deposit_end_time': prop['depositEndTime'],
                'total_deposit': int(prop['totalDeposit'][0]['amount']),
                'voting_start_time': prop['votingStartTime'],
                'voting_end_time': voting_end_time,
                'voting_end_height': voting_end_height
            }


        insert_query = (
            "INSERT INTO proposals ("
            "`id`, `@type`, title, description, status, yes, abstain, `no`,"
            "no_with_veto, submit_time, deposit_end_time, total_deposit,"
            "voting_start_time, voting_end_time, voting_end_height) "
            "VALUES (%(`id`)s, %(`@type`)s, %(title)s, %(description)s,"
            "%(status)s, %(yes)s, %(abstain)s, %(`no`)s, %(no_with_veto)s,"
            "%(submit_time)s, %(deposit_end_time)s, %(total_deposit)s,"
            "%(voting_start_time)s, %(voting_end_time)s, %(voting_end_height)s) "
            "ON DUPLICATE KEY UPDATE status = VALUES(status),"
            "yes = VALUES(yes), abstain = VALUES(abstain),"
            "`no` = VALUES(`no`), no_with_veto = VALUES(no_with_veto),"
            "total_deposit = VALUES(total_deposit);"
        )
        self.cursor.executemany(insert_query, reformatted_proposals)
        self.conn.commit()

        self.cursor.close()
        self.conn.close()

if __name__ == '__main__':
    for chain in BLOCKCHAINS:
        ProposalInfo(chain)

