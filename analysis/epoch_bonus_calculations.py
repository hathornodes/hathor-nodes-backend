import pandas as pd
import pymysql
import matplotlib.pyplot as plt
import seaborn as sns

from aws_client import get_secret_value

db_creds = get_secret_value('hathor_nodes_db')
blockchain = 'osmosis'
conn = pymysql.connect(
    host=db_creds['host'],
    user=db_creds['username'],
    password=db_creds['password'],
    db=blockchain
)
cursor = conn.cursor()

cursor.execute("""
SELECT
	moniker,
	tokens / 1000000 AS voting_power,
	100 * ROUND(commission_rate, 3) AS commission_perc,
	100 * ROUND((1-commission_rate)* 1000000 * 36500 / tokens, 3) AS delegator_epoch_bonus_perc
FROM
	`_validators` v
WHERE
	status = 3
	AND commission_rate < 1
ORDER BY
	100 * ROUND((1-commission_rate)* 1000000 * 36500 / tokens, 3) DESC;
""")

votes = cursor.fetchall()
votes = pd.DataFrame(
    votes, columns=[
        'moniker', 'voting_power', 'commission_perc', 'delegator_apr_increase'])

plt.figure(figsize = (15,8))
ax = sns.barplot(data=votes, x='moniker', y='delegator_apr_increase')
ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
plt.show()
"""
votes_wide = pd.pivot_table(
    votes, values='short_name',
    index=['proposal_id'], columns=['moniker'], aggfunc=max
)
votes_wide.reset_index(inplace=True)
print(votes_wide.tail(5))
votes_wide.to_json("validator_voting_history.json", orient='records')
"""
