"""Collect data on SFS Delegations."""
import codecs
import re
import requests

import pandas as pd

import interface.cosmos.crypto.ed25519.keys_pb2
import interface.cosmos.staking.v1beta1.query_pb2 as query_staking
import interface.cosmos.tx.v1beta1.service_pb2 as query_txs
import interface.osmosis.lockup.lock_pb2
import interface.osmosis.lockup.query_pb2 as query_locks
import interface.osmosis.gamm.v1beta1.query_pb2 as query_gamms
import interface.osmosis.superfluid.query_pb2 as query_superfluid
from utils import BlockchainAccessor

class SFSInfo(BlockchainAccessor):
    """Object for updating pools info."""
    def __init__(self, blockchain):
        super().__init__(blockchain)
        self.pool_info = {}
        sfs_txs = self._fetch_sfs_delegations()
        sfs_txs = self._calc_delegation_values(sfs_txs)
        sfs_txs = pd.DataFrame(sfs_txs)
        agg_data = sfs_txs.groupby('moniker').agg({'delegator_address': 'count', 'osmo_amount': ['sum', 'min', 'max']})
        sfs_txs.to_csv("data/sfs_test_data.csv", index=False)
        agg_data.to_csv("data/agg_sfs_test_data.csv")

    def _fetch_sfs_delegations(self) -> list:
        """Query all sfs delegations."""
        request_msg = query_staking.QueryValidatorsRequest()
        request_msg.pagination.limit = 10000
        validators = self._send_abci_query(
            request_msg, '/cosmos.staking.v1beta1.Query/Validators',
            query_staking.QueryValidatorsResponse, 0)['validators']
 
        sfs_txs = []
        url = f'{self.config["lcd"]}/osmosis/superfluid/v1beta1/all_assets'
        sfs_assets = requests.get(url).json()['assets']
        for validator in validators:
            for asset in sfs_assets:
                request_msg = query_superfluid.SuperfluidDelegationsByValidatorDenomRequest()
                request_msg.validator_address = validator['operatorAddress']
                request_msg.denom = asset['denom']
                sfs_delegators = self._send_abci_query(
                    request_msg, '/osmosis.superfluid.Query/SuperfluidDelegationsByValidatorDenom',
                    query_superfluid.SuperfluidDelegationsByValidatorDenomResponse, 0)
                sfs_delegators = sfs_delegators.get('superfluidDelegationRecords', [])

                for delegator in sfs_delegators:
                    sfs_txs.append({
                        'moniker': validator['description']['moniker'],
                        'operator_address': validator['operatorAddress'],
                        'delegator_address': delegator['delegatorAddress'],
                        'denom': delegator['delegationAmount']['denom'],
                        'amount': delegator['delegationAmount']['amount']
                    })
        return sfs_txs

    def _calc_delegation_values(self, delegations: list) -> list:
        """Query Num Gamms in each SFS Lockup & calc # Osmos underlying."""
        for delegation in delegations:
            gamms = int(delegation['amount'])
            pool_id = re.search(
                '(?<=gamm/pool/)[0-9]*', delegation['denom']).group(0)
            if pool_id in self.pool_info:
                pool_info = self.pool_info[pool_id]
            else:
                url = f"{self.config['lcd']}/osmosis/gamm/v1beta1/pools/{pool_id}"
                pool_info = requests.get(url).json()['pool']
                self.pool_info[pool_id] = pool_info
            total_gamms = int(pool_info['totalShares']['amount'])

            for asset in pool_info['poolAssets']:
                if asset['token']['denom'] == 'uosmo':
                    n_osmos = int(asset['token']['amount'])/1000000
                    break

            osmos_per_gamm = n_osmos/total_gamms
            delegation['osmo_amount'] = osmos_per_gamm * gamms/2
        return delegations

if __name__ == '__main__':
    SFSInfo('osmosis')
