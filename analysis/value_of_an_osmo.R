library(dplyr)
library(ggplot2)
library(reshape2)
library(readr)

pool_tvl_data <- read_csv("~/osmo_tvl_data.csv")
cleaned_data <-
    pool_tvl_data %>%
    mutate(
        `100% Migration Price` = (osmo_tvl + 1.00*other_tvl)/bonded_osmos,
        `50% Migration Price` = (osmo_tvl + 0.50*other_tvl)/bonded_osmos,
        `Price Reduction` = (osmo_tvl * 0.8)/bonded_osmos
    ) %>%
    select(-osmo_tvl, -bonded_osmos, -other_tvl, -joint_osmo_price, -Epoch, `Current Price` = osmo_price, `Date` = `Day`)

d <- melt(cleaned_data, id.vars="Date")

ggplot(d, aes(Date, value, col=variable)) +
    geom_point() +
    stat_smooth(method = "loess", formula = "y ~ x") +
    ylab("Price of Osmo (USD)") +
    guides(col=guide_legend(title = "Situation"))

perc_tvl_osmo <-
    pool_tvl_data %>%
    select(Date = Day, osmo_tvl, other_tvl) %>%
    mutate(perc_tvl = 100*round(osmo_tvl/(osmo_tvl + other_tvl), digits=3))

ggplot(perc_tvl_osmo, aes(Date, perc_tvl)) +
    geom_point(col="red") +
    stat_smooth(method = "loess", formula = "y ~ x", col="red") +
    ylab("% of TVL in Osmo Pools")
